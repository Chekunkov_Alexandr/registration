package com.example.unnamed.colors;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.style.BackgroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import static com.example.unnamed.colors.R.color.blueColor;
import static com.example.unnamed.colors.R.color.greenColor;
import static com.example.unnamed.colors.R.color.purpleColor;
import static com.example.unnamed.colors.R.color.redColor;
import static com.example.unnamed.colors.R.color.silverColor;
import static com.example.unnamed.colors.R.color.standardColor;
import static com.example.unnamed.colors.R.color.whiteColor;
import static com.example.unnamed.colors.R.color.yellowColor;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button button;
    private RelativeLayout mRelativeLayout;
    private ContextCompat mContextCompat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mRelativeLayout = (RelativeLayout) findViewById(R.id.activity_main);


        button = (Button)findViewById(R.id.red);
        button.setOnClickListener(this);

        button = (Button)findViewById(R.id.green);
        button.setOnClickListener(this);

        button = (Button)findViewById(R.id.yellow);
        button.setOnClickListener(this);

        button = (Button)findViewById(R.id.blue);
        button.setOnClickListener(this);

        button = (Button)findViewById(R.id.silver);
        button.setOnClickListener(this);

        button = (Button)findViewById(R.id.purple);
        button.setOnClickListener(this);

        button = (Button)findViewById(R.id.white);
        button.setOnClickListener(this);

        button = (Button) findViewById(R.id.standardcolour);
        button.setOnClickListener(this);
    }

    public void onClick(View v){
        switch(v.getId()){
            //*Красный Цвет!*\\
            case R.id.red:
                mRelativeLayout.setBackgroundColor(ContextCompat.getColor(MainActivity.this, redColor));
                break;
            //*Зеленый Цвет!*\\
            case R.id.green:
                mRelativeLayout.setBackgroundColor(ContextCompat.getColor(MainActivity.this, greenColor));
                break;
            //*Желтый Цвет!*\\
            case R.id.yellow:
                mRelativeLayout.setBackgroundColor(ContextCompat.getColor(MainActivity.this, yellowColor));
                break;
            //*Синий Цвет!*\\
            case R.id.blue:
                mRelativeLayout.setBackgroundColor(ContextCompat.getColor(MainActivity.this, blueColor));
                break;
            //*Серебренный Цвет!*\\
            case R.id.silver:
                mRelativeLayout.setBackgroundColor(ContextCompat.getColor(MainActivity.this, silverColor));
                break;
            //*Фиолетовый Цвет!*\\
            case R.id.purple:
                mRelativeLayout.setBackgroundColor(ContextCompat.getColor(MainActivity.this, purpleColor));
                break;
            //*Белый Цвет!*\\
            case R.id.white:
                mRelativeLayout.setBackgroundColor(ContextCompat.getColor(MainActivity.this, whiteColor));
                break;
            case R.id.standardcolour:
                mRelativeLayout.setBackgroundColor(ContextCompat.getColor(MainActivity.this, standardColor));
                break;
            default:
                break;
        }
    }








}
